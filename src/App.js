import React from 'react'
import {Player, ControlBar} from 'video-react'
import 'video-react/dist/video-react.css';

const sources = {
  sintelTrailer: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
  bunnyTrailer: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
  bunnyMovie: 'http://media.w3.org/2010/05/bunny/movie.mp4',
  test: 'http://media.w3.org/2010/05/video/movie_300.webm',
}

function App() {
  return (
    <div>
      <Player>
        <source src={sources.bunnyMovie} />
        <ControlBar autoHide={false} />
      </Player>
    </div>
  );
}

export default App;
